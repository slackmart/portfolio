slackmart portfolio
===================

This single page application is being built using the aurelia framework.

Technology stack
------------------

- Aurelia
- Bulma
- Yarn
- Webpack
- Font Awesome

Getting started for developers
------------------------------

::

    $ git clone https://gitlab.com/slackmart/portfolio.git
    $ cd portfolio
    $ yarn install
    $ au run

Go to http://localhost:8080
