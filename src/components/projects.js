import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';

@inject(HttpClient)
export class Projects {
  activate(params, routeConfig, navigationInstruction) {
    console.log('Activate', params, routeConfig, navigationInstruction);
    return this.http.get(`${this.bitbucketBaseUrl}/repositories/slackmart?pagelen=20`)
      .then(response => response.json())
      .then(objects => {
        return objects.values.filter(repo => this.bitbucketNames.indexOf(repo.name) >= 0);
      }).then(repos => {
        this.bitbucket = repos;
        return Promise.all(this.bitbucket.map(repo => {
          let url = `${this.bitbucketBaseUrl}/repositories/slackmart/${repo.slug}/commits?pagelen=1&start=0`;
          let msg = '';
          return this.http.get(url)
            .then(response => response.json())
            .then(objects => {
              msg = objects.values[0].message;
              let uri = `${this.bitbucketBaseUrl}/repositories/slackmart/${repo.slug}/diffstat/${objects.values[0].hash}`;
              return this.http.get(uri)
            })
            .then(response => response.json())
            .then(objects => {
              repo['lastCommit'] = {'msg': msg, 'stats': objects.values};
              console.log('lastCommit.msg', repo.lastCommit.msg);
              console.log('stats', objects.values);
            })
        })
        );
      });
  }
  constructor(httpClient) {
    this.http = httpClient;
    this.bitbucketBaseUrl = 'https://api.bitbucket.org/2.0';
    this.bitbucketNames = [
      'chopchopchop', 'lmx', 'bdd_superlist', 'pyco', 'expenses', 'chat', 'interviews'
    ];

    this.http.get('https://gitlab.com/api/v4/users/slackmart/projects')
      .then(response => response.json())
      .then(objects => {
        this.gitlab = objects;
        console.log('gitlab repos', this.gitlab);
      });
  }
}
