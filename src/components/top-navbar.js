import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

@inject(Router)
export class TopNavbar {
  constructor(router) {
    console.log('Header !');
    this.router = router;
    this.networks = [
      {name: 'Email', cssclass: 'fa fa-envelope', url: 'mailto:slackmart@gmail.com'},
      {name: 'StackOverflow', cssclass: 'fa fa-stack-overflow', url: 'https://stackoverflow.com/users/977593/slackmart'},
      {name: 'Gitlab', cssclass: 'fa fa-gitlab', url: 'https://gitlab.com/slackmart'},
      {name: 'Bitbucket', cssclass: 'fa fa-bitbucket', url: 'https://bitbucket.org/slackmart'},
      {name: 'Google Plus', cssclass: 'fa fa-google-plus', url: 'https://plus.google.com/+MartinRodriguezGuerrero'}
    ];
  }
}
