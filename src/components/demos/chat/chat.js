export class Chat {
  constructor() {
    let wsScheme = window.location.protocol == "https:" ? "wss" : "ws";
    this.socketUrl = wsScheme + '://' + 'localhost' + ':8000' + '/chat/';
    this.message = '';
    this.username = '';
    this.socket = null;
    this.messages = [];
    this.users = [];
  }

  onmessage = (response) => {
    let data = JSON.parse(response.data);
    // console.log('Message data', data);
    this.messages.push(data);

    console.log('Username:', data.username);
    console.log('Users:', this.users);
    if (this.users.indexOf(data.username) == -1) {
      this.users.push(data.username);
    }
    this.usersNotEmpty = true;
    this.messagesNotEmpty = true;
  }

  submit() {
    this.usernameSaved = this.username ? true: false;
    if (this.usernameSaved) {
      if (this.socket == null) {
        this.socket = new WebSocket(this.socketUrl);
        this.socket.onclose = (reason) => console.log('On close', reason);
        this.socket.onerror = (reason) => console.log('On error', reason);
        this.socket.onmessage = this.onmessage;
        this.socket.onopen = (response) => console.log('Socket Open', response);
      }
      if (this.message !== '') {
        this.socket.send(JSON.stringify({username: this.username, message: this.message}));
        console.log('Message Submited!')
        this.message = '';
      }
    }
  }
}
