import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';
import {Resource} from '../../../services/resource';

@inject(Resource, DialogController)
export class Form {
  activate(model) {
    this.resourceName = model.resourceName;
    this.fields = model.fields;
    this.options = model.options;
  }
  setType(field) {
    return {
      'date': 'date',
      'concept': 'text',
      'name': 'text',
      'amount': 'number'
    }[field];
  }
  async submit() {
    console.log('submited', this.object);
    let answer = await this.resource.post(this.resourceName, this.object);
    this.dialogController.ok(answer);
  }
  constructor(resource, dialogController) {
    this.resource = resource;
    this.dialogController = dialogController;
    this.object = {};
  }
}
