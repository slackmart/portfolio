import {inject} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {Form} from './form';
import {Resource} from '../../../services/resource';

@inject(Resource, DialogService)
export class Generic {
  async activate() {
    this.data = new Map();
    this.data.set('ingresses', { name: 'ingresses', fields: ['date', 'concept', 'amount', 'target'] });
    this.data.set('egresses', { name: 'egresses', fields: ['date', 'concept', 'amount', 'target'] });
    this.data.set('storages', {
      name: 'storages',
      fields: ['storage_type', 'name', 'amount'],
      showActions: true,
      options: [
        {value: 'd', name: 'Debit'},
        {value: 'c', name: 'Credit'}
      ]
    });
    await this.fetchStorages();
  }

  constructor(resource, dialogService) {
    this.resource = resource;
    this.dialog = dialogService;
  }

  async fetchObjects(resourceName) {
    console.log('Fetching latest', resourceName);
    return await this.resource.fetchAll(resourceName);
  }

  async fetchStorages() {
    let storages = await this.resource.fetchAll('storages');
    let hasStorages = storages.length > 0;
    let options = storages.map(item => {
      item.value = item.id;
      return item;
    });
    for (var resource of ['ingresses', 'egresses']) {
      this.data.get(resource).options = options;
      this.data.get(resource).showActions = hasStorages;
    }
    this.rerender = hasStorages;
    return options;
  }

  async newItem(resourceName) {
    console.log('Net Item', resourceName);
    this.dialog.open({
      viewModel: Form,
      lock: false,
      model: {
        resourceName: resourceName, fields: this.data.get(resourceName).fields,
        options: this.data.get(resourceName).options
      }
    }).then(openDialogResult => openDialogResult.closeResult).then(async (response) => {
        if (!response.wasCancelled) {
          if (resourceName === 'storages') {
            await this.fetchStorages();
          } else {
            this.data.get(resourceName).objects = await this.fetchObjects(resourceName);
            console.log('HERE', this.data.get(resourceName).objects);
          }
        }
      });
  }
}
