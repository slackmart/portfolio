import {inject} from 'aurelia-framework';
import {Resource} from '../../../services/resource';

@inject(Resource)
export class ItemsList {
  activate(model) {
    this.resourceName = model.name;
    this.fields = model.fields;
    this.objects = model.objects;
    this.showActions = model.showActions;
  }
  constructor(resource) {
    this.resource = resource;
  }
  async fetch() {
    this.objects = await this.resource.fetchAll(this.resourceName);
    console.log('Fetched: ', this.objects);
  }
}
