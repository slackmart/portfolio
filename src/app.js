import {PLATFORM} from 'aurelia-pal';

export class App {
  constructor() {}

  configureRouter(config, router) {
    this.router = router;
    config.title = 'Slackmart Portfolio';
    config.map([
      {
        route: ['', 'about-me'], name: 'aboutme', nav: true, title: 'About',
        viewPorts: {
          'main-content': {moduleId: PLATFORM.moduleName('components/about-me')},
          'hero-body': {moduleId: PLATFORM.moduleName('components/me')}
        }
      },
      {
        route: 'projects', name: 'projects', nav: true, title: 'Projects',
        viewPorts: {
          'main-content': {moduleId: PLATFORM.moduleName('components/projects')},
          'hero-body': {moduleId: PLATFORM.moduleName('components/empty')}
        }
      },
      {
        route: 'demos', name: 'demos', nav: true, title: 'Demos',
        viewPorts: {
          'main-content': {moduleId: PLATFORM.moduleName('components/demos/demo-list')},
          'hero-body': {moduleId: PLATFORM.moduleName('components/empty')}
        }
      },
      {
        route: 'free-time', name: 'free-time', nav: true, title: 'Freedom',
        viewPorts: {
          'main-content': {moduleId: PLATFORM.moduleName('components/free-time')},
          'hero-body': {moduleId: PLATFORM.moduleName('components/empty')}
        }
      }
    ]);
  }
}
