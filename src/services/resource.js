import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import environment from '../environment';

@inject(environment, HttpClient)
export class Resource {
  constructor(environ, httpClient) {
    this.http = httpClient;
    console.log('Environment', environ);
    this.http.configure(config => config.withBaseUrl(environ.apiUrl));
  }

  async fetchAll(resourceName) {
    return await this.http.fetch(`/${resourceName}/`)
      .then(response => response.json())
      .then(objects => objects)
      .catch(err => console.error('ERROR', err));
  }

  async post(resourceName, object) {
    return this.http.fetch(`/${resourceName}/`, {
        method: 'post',
        body: json(object),
        headers: {'Content-Type': 'application/json', 'Accept': 'application/json'}
      })
      .then(response => response)
      .catch(err => console.error(err));
  }
}
