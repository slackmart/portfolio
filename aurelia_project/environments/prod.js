export default {
  debug: false,
  testing: false,
  apiUrl: 'https://accountify.cfapps.io/api'
};
